import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalCountComponent } from './animal-count.component';

describe('AnimalCountComponent', () => {
  let component: AnimalCountComponent;
  let fixture: ComponentFixture<AnimalCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
