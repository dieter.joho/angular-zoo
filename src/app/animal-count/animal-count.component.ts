import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Animal} from '../animal.model';

@Component({
  selector: 'app-animal-count',
  templateUrl: './animal-count.component.html',
  styleUrls: ['./animal-count.component.css']
})
export class AnimalCountComponent implements OnInit {
  @Input() animal: Animal;
  @HostBinding('attr.class') cssClass = 'four wide column center align votes';

  constructor() {
  }

  ngOnInit() {
  }

  breed(): boolean {
    this.animal.breed();
    return false;
  }

  shoot(): boolean {
    this.animal.shoot();
    return false;
  }
}
