import {Component, Input, OnInit, Output} from '@angular/core';
import {Animal} from '../animal.model';
import {EventEmitter} from '@angular/core';

@Component({
  selector: 'app-zoo',
  templateUrl: './zoo.component.html',
  styleUrls: ['./zoo.component.css']
})
export class ZooComponent implements OnInit {
  @Input() animals: Animal[];

  @Output() onAnimalSelected: EventEmitter<Animal>;

  currentAnimal: Animal;

  constructor() {
    this.onAnimalSelected = new EventEmitter<Animal>();
  }

  ngOnInit() {
  }

  sortedAnimals(): Animal[] {
    return this.animals.sort((a: Animal, b: Animal) => b.count - a.count);
  }

  addAnimal(name: HTMLInputElement, imageUrl: HTMLInputElement, species: HTMLSelectElement, count: HTMLInputElement): boolean {
    this.animals.push(new Animal(this.animals.length + 1, name.value, imageUrl.value, species.value.split(' / '), Number(count.value)));
    console.log(`Adding animal name: ${name.value} and image: ${imageUrl.value}`);
    name.value = '';
    imageUrl.value = '';
    count.value = null;
    return false;
  }

  deleteCurrentAnimal() {
    const deleteIndex = this.animals.indexOf(this.currentAnimal);
    console.log(deleteIndex);
    if (deleteIndex >= 0) {
      this.animals.splice(deleteIndex, 1);
      this.currentAnimal = null;
    }
    return false;
  }

  clicked(animal: Animal): void {
    this.currentAnimal = animal;
    this.onAnimalSelected.emit(animal);
  }

  isSelected(animal: Animal): boolean {
    if (!animal || !this.currentAnimal) {
      return false;
    }
    return animal.id === this.currentAnimal.id;
  }

}
