import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZooComponent } from './zoo/zoo.component';
import { AnimalComponent } from './animal/animal.component';
import { AnimalImageComponent } from './animal-image/animal-image.component';
import { SpeciesComponent } from './species/species.component';
import { AnimalCountComponent } from './animal-count/animal-count.component';

@NgModule({
  declarations: [
    AppComponent,
    ZooComponent,
    AnimalComponent,
    AnimalImageComponent,
    SpeciesComponent,
    AnimalCountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
