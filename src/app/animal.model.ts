export class Animal {
  constructor(
    public id: number,
    public name: string,
    public imageUrl: string,
    public species?: string[],
    public count?: number
  ) {
  }

  breed(): void {
    const kids: number = Math.floor(Math.random() * (+5 - +1)) + +1;
    this.count += kids;
  }

  shoot(): void {
    if (this.count > 0) {
      this.count -= 1;
    }
  }
}
