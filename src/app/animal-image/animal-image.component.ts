import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Animal} from '../animal.model';

@Component({
  selector: 'app-animal-image',
  templateUrl: './animal-image.component.html',
  styleUrls: ['./animal-image.component.css']
})
export class AnimalImageComponent implements OnInit {
  @Input() animal: Animal;
  @HostBinding('attr.class') cssClass = 'four wide column';

  constructor() { }

  ngOnInit() {
  }

}
