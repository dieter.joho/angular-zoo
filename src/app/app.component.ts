import {Component} from '@angular/core';
import {Animal} from './animal.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  animals: Animal[];

  constructor() {
    this.animals = [
      new Animal(
        1,
        'Löwe',
        'https://www.diamond-painting-deutschland.de/images/product_images/original_images/L%C3%B6we.png',
        ['Säugetiere', 'Fleischfresser', 'Grosskatzen'],
        2),
      new Animal(
        2,
        'Zebra',
        'https://theearthorganization.org/wp-content/uploads/2016/04/zebra.jpg',
        ['Säugetiere', 'Pflanzenfresser', 'Huftiere'],
        12),
      new Animal(
        3,
        'Hammerhai',
        'https://www.prowildlife.de/wp-content/uploads/2017/07/hammerhai-meer-768x576.jpg',
        ['Fische'],
        3),
      new Animal(
        4,
        'Vogel Strauss',
        'https://cdn.pixabay.com/photo/2019/02/03/15/55/bouquet-3972867_1280.jpg',
        ['Vögel'],
        2),
      new Animal(
        5,
        'Kobra',
        'https://cdn.duden.de/_media_/small/K/Kobra-201020375799.jpg',
        ['Reptilien'],
        1),
      new Animal(
        6,
        'Clown Fisch',
        'https://petco.scene7.com/is/image/PETCO/151203-center-1?$ProductDetail-large$',
        ['Fische'],
        45)
    ];
  }
}
