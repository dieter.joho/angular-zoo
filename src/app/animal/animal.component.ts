import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {Animal} from '../animal.model';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {
  @Input() animal: Animal;

  @HostBinding('attr.class') cssClass = 'ui item grid';

  constructor() { }

  ngOnInit() {
  }

}
